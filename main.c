
/*************************************************************************************************
 * 					ITS-PLC Startup Project
 *
 * 					(c) Polytech Montpellier - Syst�mes Embarqu�s
 *
 * 					17/02/2015 - Glenisson Cavallera
 *
 * 					FreeRTOS getting started project to work with ITS-PLC Simulator
 *
 * 					Palletizer Software Development
 *
 *************************************************************************************************/
// Include main headers

#include "stm32f4xx.h"
#include "fonts.h"

// Include BSP headers

#include "stm32f429i_discovery.h"
#include "stm32f429i_discovery_lcd.h"
#include "stm32f429i_discovery_sdram.h"
#include "stm32f429i_discovery_ioe.h"

// Include FreeRTOS headers

#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "queue.h"

#define DBG_OFFLINE	0
#define	DBG_ONLINE	1
// Local defines
#define STATE_0 0x10
#define STATE_1 0x11
#define STATE_2 0x12
#define STATE_3 0x13
#define STATE_4 0x14

#define TRUE 0x01
#define FALSE 0x00

#define CAPTEUR_0 0x01
#define CAPTEUR_1 0x02
#define CAPTEUR_2 0x04
#define CAPTEUR_3 0x08
#define CAPTEUR_4 0x10
#define CAPTEUR_5 0x20
#define CAPTEUR_6 0x40
#define CAPTEUR_7 0x80
#define CAPTEUR_8 0x100
#define CAPTEUR_9 0x200
#define CAPTEUR_10 0x400

#define ACTIONNEUR_0_ON		0x201
#define ACTIONNEUR_1_ON		0x202
#define ACTIONNEUR_2_ON		0x204
#define ACTIONNEUR_3_ON		0x208
#define ACTIONNEUR_4_ON		0x210
#define ACTIONNEUR_5_ON		0x220
#define ACTIONNEUR_6_ON		0x240
#define ACTIONNEUR_7_ON		0x280

#define ACTIONNEUR_0_OFF	0x101
#define ACTIONNEUR_1_OFF	0x102
#define ACTIONNEUR_2_OFF	0x104
#define ACTIONNEUR_3_OFF	0x108
#define ACTIONNEUR_4_OFF	0x110
#define ACTIONNEUR_5_OFF	0x120
#define ACTIONNEUR_6_OFF	0x140
#define ACTIONNEUR_7_OFF	0x180

#define FRONT_MONTANT		0x05
#define FRONT_DESCENDANT	0x06



// Include main headers
#include "stm32f4xx.h"
#include "fonts.h"

// Include BSP headers
#include "stm32f429i_discovery.h"
#include "stm32f429i_discovery_lcd.h"
#include "stm32f429i_discovery_sdram.h"
#include "stm32f429i_discovery_ioe.h"

// Include FreeRTOS headers
#include "FreeRTOSConfig.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "semphr.h"
#include "queue.h"

// Declare Tasks
void vTaskElevateurPoussoir (void *pvParameters);
void vTaskGlissiere (void *pvParameters);
void vTaskButoir (void *pvParameters);
void vTaskPinces (void *pvParameters);
void vTaskAscenseur (void *pvParameters);
void vTaskTapisPalette (void *pvParameters);
void vTaskEntree (void *pvParameters);
void vTaskSortie (void *pvParameters);

// Trace Labels
traceLabel user_event_channel;



// Global variables
static	uint16_t	   its_sensors;			// 16 bits representing sensors states from ITS-PLC
static	uint16_t	   forced_sensors;		// 16 bits representing sensors states from User Interface
static	uint16_t	   sensors;				// Bitwise OR between the two above
static	uint16_t	   actuators;			// 16 bits representing actuators states

static  uint8_t		   dbg_mode;			// ONLINE/OFFLINE upon detection of the I/O Extender on boot

static uint16_t 	Buffer_Entree_A =0x00;
static uint16_t		Buffer_Entree_C = 0x00;
static uint16_t 	Buffer_Entree = 0x00;

// Handle
SemaphoreHandle_t xSyncEnvoyer2Cartons;
SemaphoreHandle_t xSync2CartonsInjectes;
SemaphoreHandle_t xSyncOkEmpiler;
SemaphoreHandle_t xSyncOuvrirGlissiere;
SemaphoreHandle_t xSyncFermerButoir;
SemaphoreHandle_t xSyncFermerPinces;
SemaphoreHandle_t xSyncPincesFermees;
SemaphoreHandle_t xSyncGlissiereRentree;
SemaphoreHandle_t xSyncCartonDeposes;
SemaphoreHandle_t xSyncAscenseurOk;
SemaphoreHandle_t xSyncPaletteOk;
SemaphoreHandle_t xSyncPalettePleine;
SemaphoreHandle_t xSemaphoreI2C;

TaskHandle_t      vHTaskElevateurPoussoir;
TaskHandle_t      vHTaskGlissiere;
TaskHandle_t      vHTaskButoir;
TaskHandle_t      vHTaskPinces;
TaskHandle_t      vHTaskAscenseur;
TaskHandle_t      vHTaskTapisPalette;
TaskHandle_t      vHTaskEntree;
TaskHandle_t      vHTaskSortie;

//Handle
BaseType_t xHandleElevateurPoussoir;
BaseType_t xHandleTaskGlissiere;
BaseType_t xHandleTaskButoir;
BaseType_t xHandleTaskPinces;
BaseType_t xHandleTaskAscenseur;
BaseType_t xHandleTaskTapisPalette;
BaseType_t xHandleEntree;
BaseType_t xHandleSortie;

QueueHandle_t     xQueueSortie;
// Main program
int main(void)
{
	// Init board LEDS and Blue Push Button

	STM_EVAL_LEDInit(LED3);
	STM_EVAL_LEDInit(LED4);
	STM_EVAL_PBInit(BUTTON_USER, BUTTON_MODE_GPIO);

    // Start IO Extenders (Touchpad & PCF IO Extenders)

    IOE_Config();

    // Test if ITS-PLC I/O Extender is connected

    if (I2C_WritePCFRegister(PCF_B_WRITE_ADDR, (int8_t)0x00))
    {
    	// I/O Extender found, ONLINE mode -> Green LED

    	dbg_mode = DBG_ONLINE;
    	STM_EVAL_LEDOn(LED3);
    	STM_EVAL_LEDOff(LED4);
    }
    else
    {
    	// I/O Extender not found, OFFLINE mode -> Red LED

    	dbg_mode = DBG_OFFLINE;
    	STM_EVAL_LEDOff(LED3);
    	STM_EVAL_LEDOn(LED4);
	}


	// Launch FreeRTOS Trace recording

	vTraceInitTraceData();
	uiTraceStart();
	user_event_channel = xTraceOpenLabel("UEV");


	// Create Semaphores
	xSyncEnvoyer2Cartons = xSemaphoreCreateBinary();
	if( xSyncEnvoyer2Cartons == NULL )
	{return;}

	xSync2CartonsInjectes = xSemaphoreCreateBinary();
	if( xSync2CartonsInjectes == NULL )
	{return;}

	xSyncOkEmpiler = xSemaphoreCreateBinary();
	if( xSyncOkEmpiler == NULL )
	{return;}

	xSyncOuvrirGlissiere = xSemaphoreCreateBinary();
	if( xSyncOuvrirGlissiere == NULL )
	{return;}

	xSyncFermerButoir = xSemaphoreCreateBinary();
	if( xSyncFermerButoir == NULL )
	{return;}

	xSyncFermerPinces = xSemaphoreCreateBinary();
	if( xSyncFermerPinces == NULL )
	{return;}

	xSyncPincesFermees = xSemaphoreCreateBinary();
	if( xSyncPincesFermees == NULL )
	{return;}

	xSyncGlissiereRentree = xSemaphoreCreateBinary();
	if(xSyncGlissiereRentree == NULL )
	{return;}

	xSyncCartonDeposes = xSemaphoreCreateBinary();
	if(xSyncCartonDeposes == NULL )
	{return;}

	xSyncAscenseurOk = xSemaphoreCreateBinary();
	if(xSyncAscenseurOk == NULL )
	{return;}

	xSyncPaletteOk =xSemaphoreCreateBinary();
	if(xSyncPaletteOk == NULL )
	{return;}

	xSyncPalettePleine =xSemaphoreCreateBinary();
	if(xSyncPalettePleine == NULL )
	{return;}

	// Create Mutex
	xSemaphoreI2C = xSemaphoreCreateMutex(); // A Ajouter controle de cr�ation
	if(xSemaphoreI2C == NULL )
	{return;}

	xQueueSortie = xQueueCreate(10, sizeof(uint16_t));
	if (xQueueSortie == NULL)
	{return;}


	//Init Task
	xHandleElevateurPoussoir = xTaskCreate(vTaskElevateurPoussoir,   "TaskElevateurPoussoir",   128, 1, 1, &vHTaskElevateurPoussoir);		// 128 bytes stack, priority 1, Pas de param�tres
	if( xHandleElevateurPoussoir != pdPASS)
	{return;}

	xHandleTaskGlissiere =     xTaskCreate(vTaskGlissiere,   "TaskGlissiere",   128, 1, 1, &vHTaskGlissiere);
	if(xHandleTaskGlissiere != pdPASS)
	{return;}

	xHandleTaskButoir =        xTaskCreate(vTaskButoir, "TaskButoir", 128, 1, 1, &vHTaskButoir);
	if( xHandleTaskButoir != pdPASS)
	{return;}

	xHandleTaskPinces =        xTaskCreate(vTaskPinces,   "TaskPinces",   128, 1, 1, &vHTaskPinces);
	if( xHandleTaskPinces != pdPASS)
	{return;}

	xHandleTaskAscenseur =     xTaskCreate(vTaskAscenseur,   "TaskAscenseur",   128, 1, 1, &vHTaskAscenseur);
	if( xHandleTaskAscenseur != pdPASS)
	{return;}

	xHandleTaskTapisPalette=   xTaskCreate(vTaskTapisPalette, "TaskTapisPalette", 128, 1, 1, &vHTaskTapisPalette);
	if( xHandleTaskTapisPalette != pdPASS)
	{return;}

	xHandleEntree =            xTaskCreate(vTaskEntree,   "TaskEntree",   128, 1, 4,&vHTaskEntree);
	if( xHandleEntree != pdPASS)
	{return;}

	xHandleSortie =            xTaskCreate(vTaskSortie,   "TaskSortie",  128, 1, 4, &vHTaskSortie);
	if( xHandleSortie != pdPASS)
	{return;}

	// Start the Scheduler
	vTaskStartScheduler();

    while(1)
    {
    	// The program should never be here...
    }
}


void vTaskElevateurPoussoir (void *pvParameters)
{
	static uint8_t STATE;
	static uint8_t p2;
	static uint8_t p3;
	static uint32_t ulNotifiedValue;
	static uint16_t message;

	STATE = STATE_0;
	p2 = TRUE;
	p3 = FALSE;

	for(;;)
	{


	switch(STATE)
	{
		case (STATE_0) :
		// ACTIONNEUR 0 activ�
		message = ACTIONNEUR_0_ON;
		xQueueSend(xQueueSortie, &message, portMAX_DELAY);


		// Attente que C0 valid�
		do
		{
			vTracePrintF(user_event_channel, "Hello from task 1");
			xTaskNotifyWait(0x00, CAPTEUR_0,&ulNotifiedValue,portMAX_DELAY);
		}while ((ulNotifiedValue & CAPTEUR_0) != CAPTEUR_0);

		message = ACTIONNEUR_0_OFF;
		xQueueSend(xQueueSortie, &message, portMAX_DELAY);
		STATE = STATE_1;
		break;

		case (STATE_1) :
		message = ACTIONNEUR_1_ON;
		xQueueSend(xQueueSortie, &message, portMAX_DELAY);
		//Attente que C1 valid�
		do
		{
			xTaskNotifyWait(0x00, CAPTEUR_1,&ulNotifiedValue,portMAX_DELAY);
		}while ((ulNotifiedValue & CAPTEUR_1) != CAPTEUR_1);

		message = ACTIONNEUR_1_OFF;
		xQueueSend(xQueueSortie, &message, portMAX_DELAY);
		STATE = STATE_2;
		break;

		case (STATE_2) :
		if(p2 == 0x01)
		{
			p2 = 0x00;
			p3 = 0x01;

			//Attente que C2 soit valid�
			do
			{
				xTaskNotifyWait(0x00, CAPTEUR_2,&ulNotifiedValue,portMAX_DELAY);
			}while ((ulNotifiedValue & CAPTEUR_2) != CAPTEUR_2);

			STATE = STATE_0;
			break;
		}
		else if(p3 == 0x01)
		{
			p2 = 0x01;
			p3 = 0x00;

			//Attente que C2 soit valid�
			do
			{
				xTaskNotifyWait(0x00, CAPTEUR_2,&ulNotifiedValue,portMAX_DELAY);
			} while ((ulNotifiedValue & CAPTEUR_2) != CAPTEUR_2);
			xSemaphoreGive(xSync2CartonsInjectes);
			STATE = STATE_3;
			break;
		}

		case (STATE_3):
			xSemaphoreTake(xSyncEnvoyer2Cartons,portMAX_DELAY);
			STATE = STATE_0;
			break;

		default :
			return;
			break;
	}
	}
}

void vTaskButoir(void *pvParameters)
{
	static uint8_t STATE;
	static uint16_t message;
	STATE = STATE_0;

	for(;;)
	{
	switch(STATE)
		{
			case (STATE_0) :
			xSemaphoreTake(xSync2CartonsInjectes, portMAX_DELAY);
			xSemaphoreTake(xSyncOkEmpiler, portMAX_DELAY);
			xSemaphoreGive(xSyncOuvrirGlissiere);
			STATE = STATE_1;
			break;

			case (STATE_1):
			message = ACTIONNEUR_2_ON;
			xQueueSend(xQueueSortie, &message, portMAX_DELAY);
			xSemaphoreTake(xSyncFermerButoir,portMAX_DELAY);
			xSemaphoreGive(xSyncEnvoyer2Cartons);
			message = ACTIONNEUR_2_OFF;
			xQueueSend(xQueueSortie, &message, portMAX_DELAY);
			STATE = STATE_0;
			break;

			default :
			return;
			break;
		}
	}
}

void vTaskGlissiere(void *pvParameters)
{
	static uint8_t STATE;
	static uint16_t message;
	static uint32_t ulNotifiedValue;

	STATE = STATE_0;

	for(;;)
	{
		switch(STATE)
		{
		case(STATE_0):
		do
		{
			xTaskNotifyWait(0x00, CAPTEUR_3, &ulNotifiedValue, portMAX_DELAY );
		} while ((ulNotifiedValue & CAPTEUR_3) != CAPTEUR_3);
		xSemaphoreTake(xSyncOuvrirGlissiere, portMAX_DELAY);
		STATE = STATE_1;
		break;

		case(STATE_1):
		message = ACTIONNEUR_3_ON;
		xQueueSend(xQueueSortie, &message, portMAX_DELAY);

		do
		{
			xTaskNotifyWait(0x00, CAPTEUR_4, &ulNotifiedValue, portMAX_DELAY );
		} while ((ulNotifiedValue & CAPTEUR_4) != CAPTEUR_4);

		xSemaphoreGive( xSyncFermerButoir );
		xSemaphoreGive( xSyncFermerPinces );
		xSemaphoreTake(xSyncPincesFermees, portMAX_DELAY);


		message = ACTIONNEUR_3_OFF;
		xQueueSend(xQueueSortie, &message, portMAX_DELAY);
		STATE = STATE_2;
		break;

		case(STATE_2):
		do
		{
			xTaskNotifyWait(0x00, CAPTEUR_3, &ulNotifiedValue, portMAX_DELAY );
		} while ((ulNotifiedValue & CAPTEUR_3) != CAPTEUR_3);
		xSemaphoreGive(xSyncGlissiereRentree);
		STATE = STATE_0;
		break;

		default:
		return;
		break;
		}
	}
}

void vTaskPinces(void *pvParameters)
{
	static uint8_t STATE;
	static uint16_t message;
	static uint32_t ulNotifiedValue;

	STATE = STATE_0;

	for(;;)
	{
		switch(STATE)
		{
		case(STATE_0):
		xSemaphoreTake(xSyncFermerPinces, portMAX_DELAY);
		STATE = STATE_1;
		break;

		case(STATE_1):
		message = ACTIONNEUR_4_ON;
		xQueueSend(xQueueSortie, &message, portMAX_DELAY);

		do
		{
			xTaskNotifyWait(0x00, CAPTEUR_5,&ulNotifiedValue,portMAX_DELAY);
		} while ((Buffer_Entree & CAPTEUR_5) != CAPTEUR_5);

		xSemaphoreGive( xSyncPincesFermees );
		STATE = STATE_2;
		break;

		case(STATE_2):

		xSemaphoreTake(xSyncGlissiereRentree, portMAX_DELAY);
		xSemaphoreGive(xSyncCartonDeposes);

		message = ACTIONNEUR_4_OFF;
		xQueueSend(xQueueSortie, &message, portMAX_DELAY);
		STATE = STATE_0;
		break;

		default:
		return;
		break;
		}
	}
}

void vTaskAscenseur(void *pvParameters)
{
	static uint8_t AnneauMemoire;
	static uint8_t STATE;
	static uint16_t message;
	static uint32_t ulNotifiedValue;

	AnneauMemoire = STATE_0;
	STATE = STATE_0;

	for(;;)
	{
	switch(STATE)
	{
	case (STATE_0) :
	message = ACTIONNEUR_6_ON;
	xQueueSend(xQueueSortie, &message, portMAX_DELAY);
	//Attente que C6 soit valid�
	do
	{
		xTaskNotifyWait(0x00, CAPTEUR_6,&ulNotifiedValue,portMAX_DELAY);
	} while ((ulNotifiedValue & CAPTEUR_6) != CAPTEUR_6);

	message = ACTIONNEUR_6_OFF;
	xQueueSend(xQueueSortie, &message, portMAX_DELAY);

	xSemaphoreGive(xSyncAscenseurOk);
	STATE = STATE_1;
	break;

	case (STATE_1) :
	xSemaphoreTake(xSyncPaletteOk, portMAX_DELAY);
	STATE = STATE_2;
	break;

	case (STATE_2) :
	message = ACTIONNEUR_5_ON;
	xQueueSend(xQueueSortie, &message, portMAX_DELAY);
	//Attente que C7 soit valid�
	do
	{
		xTaskNotifyWait(0x00, CAPTEUR_7,&ulNotifiedValue,portMAX_DELAY);
	} while ((ulNotifiedValue & CAPTEUR_7) != CAPTEUR_7);

	message = ACTIONNEUR_5_OFF;
	xQueueSend(xQueueSortie, &message, portMAX_DELAY);

	xSemaphoreGive(xSyncOkEmpiler);
	STATE = STATE_3;
	break;

	case (STATE_3) :
	xSemaphoreTake(xSyncCartonDeposes, portMAX_DELAY);
	STATE = STATE_4;
	break;

	case (STATE_4):
	message = ACTIONNEUR_6_ON;
	xQueueSend(xQueueSortie, &message, portMAX_DELAY);
	if(AnneauMemoire == STATE_0)
	{
		//Attente que C8 soit valid�
		do
		{
			xTaskNotifyWait(0x00, CAPTEUR_8,&ulNotifiedValue,portMAX_DELAY);
		} while ((ulNotifiedValue & CAPTEUR_8) != CAPTEUR_8);
		message = ACTIONNEUR_6_OFF;
		xQueueSend(xQueueSortie, &message, portMAX_DELAY);
		xSemaphoreGive(xSyncOkEmpiler);
		xSemaphoreTake(xSyncCartonDeposes, portMAX_DELAY);
		AnneauMemoire = STATE_1;
		STATE = STATE_4;
		break;
	}
	else if(AnneauMemoire == STATE_1)
	{
		//Attente que C9 soit valid�
		do
		{
			xTaskNotifyWait(0x00, CAPTEUR_9,&ulNotifiedValue,portMAX_DELAY);
		} while ((ulNotifiedValue & CAPTEUR_9) != CAPTEUR_9);
		message = ACTIONNEUR_6_OFF;
		xQueueSend(xQueueSortie, &message, portMAX_DELAY);
		xSemaphoreGive(xSyncOkEmpiler);
		xSemaphoreTake(xSyncCartonDeposes, portMAX_DELAY);
		AnneauMemoire = STATE_2;
		STATE = STATE_4;
		break;
		}
	else if(AnneauMemoire == STATE_2)
	{
		//Attente que C6 soit valid�
		do
		{
			xTaskNotifyWait(0x00, CAPTEUR_6,&ulNotifiedValue,portMAX_DELAY);
		} while ((ulNotifiedValue & CAPTEUR_6) != CAPTEUR_6);
		xSemaphoreGive(xSyncPalettePleine);
		AnneauMemoire = STATE_0;
		STATE = STATE_0;
		break;
	}
	default:
	return;
	break;
	}
	}
}

void vTaskTapisPalette(void *pvParameters)
{
	static uint8_t STATE;
	static uint16_t message;
	static uint32_t ulNotifiedValue;

	STATE = STATE_0;

	for(;;)
	{
		switch(STATE)
		{
		case(STATE_0):
		xSemaphoreTake(xSyncAscenseurOk, portMAX_DELAY);
		STATE = STATE_1;
		break;

		case(STATE_1):
		message = ACTIONNEUR_7_ON;
		xQueueSend(xQueueSortie, &message, portMAX_DELAY);
		do
		{
			xTaskNotifyWait(0x00, 0x00,&ulNotifiedValue,portMAX_DELAY);
		} while (ulNotifiedValue != FRONT_MONTANT);

		xSemaphoreGive(xSyncPaletteOk);
		message = ACTIONNEUR_7_OFF;
		xQueueSend(xQueueSortie, &message, portMAX_DELAY);
		STATE = STATE_2;
		break;

		case(STATE_2):
		xSemaphoreTake(xSyncPalettePleine,portMAX_DELAY);
		STATE = STATE_3;
		break;

		case(STATE_3):
		message = ACTIONNEUR_7_ON;
		xQueueSend(xQueueSortie, &message, portMAX_DELAY);
		do
		{
			xTaskNotifyWait(0x00,0x00,&ulNotifiedValue,portMAX_DELAY );
		} while (ulNotifiedValue != FRONT_DESCENDANT);

		message = ACTIONNEUR_7_OFF;
		xQueueSend(xQueueSortie, &message, portMAX_DELAY);
		STATE = STATE_1;
		break;

		default:
		return;
		break;
		}
	}
}

void vTaskEntree (void *pvParameters)
{

	static uint32_t     Front;

	for(;;)
	{
		xSemaphoreTake( xSemaphoreI2C, portMAX_DELAY );
		Buffer_Entree_A = I2C_ReadPCFRegister(PCF_A_READ_ADDR);
		Buffer_Entree_C = I2C_ReadPCFRegister(PCF_C_READ_ADDR);
		xSemaphoreGive( xSemaphoreI2C );

		Buffer_Entree =  ~((Buffer_Entree_C<<8) | Buffer_Entree_A);

		if((Buffer_Entree & CAPTEUR_0) == CAPTEUR_0)
	    {xTaskNotify( vHTaskElevateurPoussoir, Buffer_Entree, eSetValueWithOverwrite);}

		if((Buffer_Entree & CAPTEUR_1) == CAPTEUR_1)
		{xTaskNotify( vHTaskElevateurPoussoir, Buffer_Entree, eSetValueWithOverwrite);}

		if((Buffer_Entree & CAPTEUR_2) == CAPTEUR_2)
		{xTaskNotify( vHTaskElevateurPoussoir, Buffer_Entree, eSetValueWithOverwrite);}

		if((Buffer_Entree & CAPTEUR_3) == CAPTEUR_3)
		{xTaskNotify( vHTaskGlissiere, Buffer_Entree, eSetValueWithOverwrite);}

		if((Buffer_Entree & CAPTEUR_4) == CAPTEUR_4)
		{xTaskNotify( vHTaskGlissiere, Buffer_Entree, eSetValueWithOverwrite);}

		if((Buffer_Entree & CAPTEUR_5) == CAPTEUR_5)
		{xTaskNotify( vHTaskPinces, Buffer_Entree, eSetValueWithOverwrite);}

		if((Buffer_Entree & CAPTEUR_6) == CAPTEUR_6)
		{xTaskNotify( vHTaskAscenseur, Buffer_Entree, eSetValueWithOverwrite);}

		if((Buffer_Entree & CAPTEUR_7) == CAPTEUR_7)
		{xTaskNotify( vHTaskAscenseur, Buffer_Entree, eSetValueWithOverwrite);}

		if((Buffer_Entree & CAPTEUR_8) == CAPTEUR_8)
		{xTaskNotify( vHTaskAscenseur, Buffer_Entree, eSetValueWithOverwrite);}

		if((Buffer_Entree & CAPTEUR_9) == CAPTEUR_9)
		{xTaskNotify( vHTaskAscenseur, Buffer_Entree, eSetValueWithOverwrite);}

		if((Buffer_Entree & CAPTEUR_10) == CAPTEUR_10)
		Front = FRONT_MONTANT;
		{xTaskNotify( vHTaskTapisPalette, Front, eSetValueWithOverwrite);}

		if((Buffer_Entree | ~CAPTEUR_10) == ~CAPTEUR_10)
		Front = FRONT_DESCENDANT;
		{xTaskNotify( vHTaskTapisPalette, Front, eSetValueWithOverwrite);}

		vTaskDelay(10);
	}

}

void vTaskSortie (void *pvParameters)
{
	static uint16_t message;
	static uint16_t 	Buffer_Sortie=0;
	static uint8_t	Memo=0;

	for(;;)
	{

	xQueueReceive( xQueueSortie, &message, portMAX_DELAY );
	xSemaphoreTake( xSemaphoreI2C, portMAX_DELAY );

		if ( message <= ACTIONNEUR_7_OFF)
		{
			Buffer_Sortie = ~message;
			Buffer_Sortie = Buffer_Sortie & Memo;
			Memo = Buffer_Sortie;
			I2C_WritePCFRegister(PCF_B_WRITE_ADDR, Buffer_Sortie);
		}
		else if ( message >= ACTIONNEUR_0_ON)
		{
			Buffer_Sortie = message;
			Buffer_Sortie = Buffer_Sortie | Memo;
 			Memo = Buffer_Sortie;
			I2C_WritePCFRegister(PCF_B_WRITE_ADDR, Buffer_Sortie);
		}
		xSemaphoreGive(xSemaphoreI2C);
	}
}
